
import processing.serial.*;

final int NUM_LEDS = 60 * 5;
final int BAUD_RATE = 1000000;

Serial myPort;  // Create object from Serial class

void setup() 
{
  size(300, 300);
  
  assert(NUM_LEDS == width);
  assert(NUM_LEDS == height);
  
  String[] port_names = Serial.list();
  
  if (port_names.length == 0)
  {
    println("No serial devices were detected!");
    noLoop();
  }
  else
  {
    println("Serial ports list:");
    
    for (int i = 0; i < port_names.length; i++)
    {
      println("  port " + i + "  :  '" + port_names[i] + "'");
    }
    
    myPort = new Serial(this, port_names[0], BAUD_RATE);
    myPort.clear();
    
    frameRate(45);
    
    colorMode(RGB, 255);
  }
}

byte[] pixels_line = new byte[NUM_LEDS * 3];

int cycle = 0;
color bkg = 0;

void keyPressed()
{
  cycle = (cycle + 1) % 8;
  
  int r = ((cycle & 1) != 0 ? 255: 0);
  int g = ((cycle & 2) != 0 ? 255: 0);
  int b = ((cycle & 4) != 0 ? 255: 0);
  println(r + " " + g + " " + b);
  
  bkg = color(r, g, b);
}

float dampedMouseX;
float dampedSpeed;

void draw() 
{
  if (myPort == null) return;
  
  float dx = mouseX - pmouseX;
  float dy = mouseX - pmouseX;
  
  float ks = 0.05;
  dampedSpeed = dampedSpeed * (1.0 - ks) + sqrt(dx*dx + dy*dy) * ks;
  float speed = map(dampedSpeed, 0.0, 20.0, 0.0, 1.0);
  speed = constrain(speed, 0.0, 1.0);
  
  noStroke();
  
  //fill(0, 0, 0, 64); rect(0, 0, width, height);
  //background(0);
  
  colorMode(RGB, 255);
  background(bkg);
  
  rectMode(CORNER);
  float w = 0.5 + 15.0 * speed;//NUM_LEDS / 4;
  
  float k = 0.05;
  dampedMouseX = dampedMouseX * (1.0 - k) + mouseX * k;
  
  colorMode(HSB, 1.0);
  
  final float ts = 0.1 * millis() / 1000.0;
  for (int i = 0; i < width; i+= 15)
  {
    float x = (dampedMouseX + i) % width;
    float hue = (map(x, 0, width, 0.0, 1.0) + ts) % 1.0;
    fill(hue, 1.0, 1.0);
    rect(x - w * 0.5, 0, w, height);
  }
  
  loadPixels();
  
  for (int i = 0, j = 0; i < NUM_LEDS; i++) 
  {
    int argb = pixels[i];
    //int a = (byte)(argb >> 24) & 0xFF);
    byte r = (byte)((argb >> 16) & 0xFF);
    byte g = (byte)((argb >> 8) & 0xFF);
    byte b = (byte)(argb & 0xFF);        
    
    // Unusual LED strip color components layout: g, r, and b.
    pixels_line[j++] = g;
    pixels_line[j++] = r;
    pixels_line[j++] = b;
  }

  myPort.write(pixels_line);
  
}