
#include "FastLED.h"

// How many leds are in the strip?
#define NUM_LEDS 60 * 5

// Data pin that led data will be written out over
#define DATA_PIN 2

// Clock pin only needed for SPI based chipsets when not using hardware SPI
//#define CLOCK_PIN 8

// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

void setup() 
{
	// Sanity check delay - allows reprogramming if accidently blowing power w/leds
  delay(2000);
  
  FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);
  
  // limit my draw to 4A at 5v of power draw
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 4000);

  // Turn off the leds
  fill_solid(leds, NUM_LEDS, CRGB(0, 0, 0));
  FastLED.show();
  
  // Fast serial spped:
  // . http://forum.arduino.cc/index.php?topic=98364.0
  // . http://forum.arduino.cc/index.php?topic=21497.msg159836#msg159836
  
  Serial.begin(1000000);
  //bitSet(UCSR0A, U2X0); // Doubles the speed
  
  // About using the ESP8266:
  // . https://github.com/FastLED/FastLED/wiki/ESP8266-notes
  // . https://github.com/FastLED/FastLED/issues/367#issuecomment-275912601
}

void loop() 
{
   Serial.readBytes( (char*)leds, NUM_LEDS * 3); // Read 3 bytes x NUM_LEDS: g, r, and b.
   FastLED.show();
}

